#!/usr/bin/env python3


''' For fixed aspect raio, analyze properties dependence on filler volume fraction
'''


import os
import matplotlib.pyplot as plt
from joblib import Parallel, delayed

from utils import (extract_fillers_from_geo, filler_to_planes, planes_to_points,
                   CylinderRestored, distance, extract_bbox_from_geo, cos_angle_between_cylinders,
                   diameter, msq, find_index_of_closest_centers_cylinder)


class DirectoryContent:
    ''' Store list of files in the directory and return some of them based on prefix.
    '''
    __directory_path = None
    __directory_content = None

    def __init__(self, directory_path):
        self.__directory_path = directory_path
        self.__directory_content = os.listdir(directory_path)

    def get_fnames_by_prefix(self, prefix):
        ''' From all file names in directory, chose those which start with prefix
        i.e. files corresponding to the chosen aspect ratio
        '''
        fnames = [fname for fname in self.__directory_content if fname.startswith(prefix)]
        return [self.__directory_path + fname for fname in fnames]


def analyze_many_files(analyzer, file_names):
    ''' Run the same analyzer for all file names from fnames
    '''
    many_points = Parallel(n_jobs=len(file_names))(delayed(analyzer)(fname) for fname in file_names)
    return many_points



def plot_data(data, **kwargs):
    ''' Plot data which is given as {x: y}
    '''
    full_title = None

    x_values = list(data.keys())
    y_values = list(data.values())
    if 'find_error' in kwargs.keys():
        slope, param = msq(x_values, y_values)
        err = sum([(y_values[idx] - slope*x_values[idx])**2 for idx in range(len(x_values))])
        err /= len(x_values)
        slope = round(slope, 5)
        err = round(err, 5)
        plt.plot([min(x_values), max(x_values)],
                 [min(x_values)*slope+param, max(x_values)*slope+param])

        if 'title' in kwargs.keys():
            full_title = kwargs['title'] + f'slope={slope}, error={err}'
        else:
            full_title = f'slope={slope}, error={err}'
    plt.plot(x_values, y_values, 'ko')

    if 'xlabel' in kwargs.keys():
        plt.xlabel(kwargs['xlabel'])
    if 'ylabel' in kwargs.keys():
        plt.ylabel(kwargs['ylabel'])
    if 'title' in kwargs.keys():
        if full_title is not None:
            plt.title(full_title)
        else:
            plt.title(kwargs['title'])
    if 'out_file_name' in kwargs.keys():
        plt.savefig(kwargs['out_file_name'])
    else:
        plt.savefig('DEFAULT.png')


def closest_neighbor_correlation(fname):
    ''' For a given structure in file, for every prticle in it,
    find the correlation between the particle and its closest neighbor
    '''
    data = dict()

    fillers = extract_fillers_from_geo(fname)
    data['N'] = len(fillers)

    cyls = []
    for filler in fillers.values():
        planes = filler_to_planes(filler)
        points = planes_to_points(planes)
        cyls.append(CylinderRestored(points))

    bbox = extract_bbox_from_geo(fname)
    cosines = []

    # For particle with idx1, find a closest neighbor which would have index idx_min
    for idx1 in range(len(cyls) - 1):
        idx_min = find_index_of_closest_centers_cylinder(idx1, cyls, bbox)
        cosines.append(abs(cos_angle_between_cylinders(cyls[idx1], cyls[idx_min])))

    filler_diameter = diameter(cyls[0].top_points, bbox)
    h = distance(cyls[0].points[0], cyls[0].points[1], bbox)
    Vfil = data['N'] * (3.14 * filler_diameter**2 / 4) * h
    V = (bbox['xhi'] - bbox['xlo']) * (bbox['yhi'] - bbox['ylo']) * (bbox['zhi'] - bbox['zlo'])

    data['fi'] = Vfil / V
    data['cosines'] = cosines

    return data


def gyration(fname):
    ''' For a given structure in file, for every prticle in it,
    find something like gyration radius of a structure made by particle and its closest neighbor
    '''
    data = dict()

    fillers = extract_fillers_from_geo(fname)
    data['N'] = len(fillers)

    cyls = []
    for f in fillers.values():
        planes = filler_to_planes(f)
        points = planes_to_points(planes)
        cyls.append(CylinderRestored(points))

    bbox = extract_bbox_from_geo(fname)
    g = 0
    for idx1, cyl1 in enumerate(cyls):
        idx_min = find_index_of_closest_centers_cylinder(idx1, cyls, bbox)  # closest neighbor
        #cyl1 = cyls[idx1]
        cyl2 = cyls[idx_min]
        pts1 = cyl1.points
        pts2 = cyl2.points
        g_current = 0
        for pt1 in pts1:
            for pt2 in pts2:
                g_current += distance(pt1, pt2, bbox)**2
        g += (g_current / (len(pts1) * len(pts2)))**0.5

    filler_diameter = diameter(cyls[0].top_points, bbox)
    h = distance(cyls[0].points[0], cyls[0].points[1], bbox)
    Vfil = data['N'] * (3.14 * filler_diameter**2 / 4) * h
    V = (bbox['xhi'] - bbox['xlo']) * (bbox['yhi'] - bbox['ylo']) * (bbox['zhi'] - bbox['zlo'])
    data['fi'] = Vfil / V
    data['gyration'] = g / data['N']

    return data


def calculate_neighbor_correlation(fnames):
    ''' For a given file names seria,
    find average closest neighbors correlation dependence on volume fraction
    '''
    many_points = analyze_many_files(closest_neighbor_correlation, fnames)
    average_data = dict()

    for pt in many_points:
        key = pt['fi'] #round(pt['fi'], 8)
        if key < 2e-3:
            continue
        value = pt['cosines']
        try:
            average_data[key].extend(value)
        except KeyError:
            average_data[key] = value

    for k in average_data:
        average_data[k] = sum(average_data[k]) / len(average_data[k])

    print('Data items:', len(average_data))
    if len(average_data) == 0:
        return None
    ar = fnames[0].split('/')[-1].split('_')[0][2:]
    plot_data(average_data, xlabel='volume fraction', ylabel=r'$\alpha$', find_error=True,
              out_file_name='closest_neighbor_correlation_' + f'ar{ar}.png',
              title=f'Correlation With Closest Neighbor ar={ar}\n' +  f'files={len(many_points)} ')
    return None


def calculate_gyration(fnames):
    ''' For a given file names seria,
    find average gyration dependence on volume fraction
    '''
    many_points = analyze_many_files(gyration, fnames)
    average_data = dict()

    ar = fnames[0].split('/')[-1].split('_')[0][2:]

    for pt in many_points:
        key = pt['fi'] #round(pt['fi'], 8)
        if key < 5e-3:
            continue
        value = pt['gyration']
        try:
            average_data[key].append(value)
        except KeyError:
            average_data[key] = [value]

    for k in average_data:
        average_data[k] = sum(average_data[k]) / len(average_data[k]) / float(ar)

    print('Data items:', len(average_data))
    if len(average_data) == 0:
        return None

    plot_data(average_data, xlabel='volume fraction', ylabel=r'$\alpha$', find_error=True,
              out_file_name='gyration_' + f'ar{ar}.png',
              title=f'Gyration ar={ar}\n'
              +f'files={len(many_points)} ')
    return None


if __name__ == '__main__':
    DIR = '../placement_analysis/geos_structures/'
    TEST_PREFIX = 'ar90_tau0.1'

    dc = DirectoryContent(DIR)
    test_fnames = dc.get_fnames_by_prefix(TEST_PREFIX)

    # Run of following
    calculate_neighbor_correlation(test_fnames)
    #calculate_gyration(test_fnames)
    