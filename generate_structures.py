#!/usr/bin/env python3


'''
Generate a lot of files for the analysis of the filler particles correlations etc.

This script runs: ./ternary_pc -N 1 demo -P 16 -r 10 -R 11 -h 1 -H 2 -L 40 [-F 0.3 -I -0.3]
'''


import subprocess
import time


DIR = './geos_structures/'  # Results go here
EXE = '/home/anton/DEV/polyhedra2/ternary_pc'  # exe which is runned


def make_files(ars):
    ''' Generate a lot of files in geo format
        to analyze positional correlations in them later
    '''

    # Default values shown to be ok for this task
    P = 16
    tau = 0.1
    h = 0.1
    LRratio = 20

    H = h * (1 + 2*tau)

    args = [f'-P{P}', f'-h{h}', f'-H{H}']

    for ar in ars:
        r = h * ar
        R = r + h*tau
        L = LRratio * R
        V = L**3
        ar_args = args + [f'-r{r}', f'-R{R}', f'-L{L}']
        for N in list(range(10, 10000))[::10]:
            Vfil = N * (3.14 * r**2) * h
            fi = Vfil / V
            if fi < 0.0025:
                continue
            if fi > 0.015:
                print(f'---------- ar = {ar}, fi = {fi}, N = {N} EXCEEDING LIMIT!!! ----------')
                continue
            print(f'ar={ar} N={N} fi={fi} {time.asctime()}')
            tag = '_'.join(time.asctime().replace(':', ' ').split())
            out_fname = DIR + f'ar{ar}_tau{tau}_N{N}_tag{tag}.geo'
            tmp_args = ar_args + [f'-N{N}', f'-O{out_fname}']
            t_start = time.time()
            subprocess.call([EXE, *tmp_args], stdout=open('tmp', 'w'))
            t_end = time.time()
            if (t_end - t_start) < 1:
                time.sleep(1)


if __name__ == '__main__':
    for count in range(10):
        for test_ar in [100]:
            make_files((test_ar,))
