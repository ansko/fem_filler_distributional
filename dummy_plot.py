#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 17:55:33 2021

@author: anton
"""

import matplotlib.pyplot as plt
#import math

from utils import msq


# beta = d(closest_neighbors_corr)/dphi
#slopes = {10: 2.95, 20: 5.94, 30: 8.56, 40: 11.24, 50: 13.08, 60: 16.25, 70: 18.73, 80: 23.98, 90: 24.2, 100: 27.97}

slopes = {10: -2.79, 20: -3.92, 30: -2.63, 40: -2.90, 50: -3.50, 60: -3.14, 70: -3.94, 80: -4.82, 90: -4.05, 100: -4.48}
          
a, b = msq(list(slopes.keys()), list(slopes.values()))

line_pts = plt.plot(list(slopes.keys()), list(slopes.values()), 'ko', label='FEM')
line_inter = plt.plot([min(slopes.keys()), max(slopes.keys())],
                      [min(slopes.keys())*a + b, max(slopes.keys())*a +b], 'k',
                      label='Interpolation')

plt.legend()

plt.savefig('closest_neighbor_correlations_vs_ars.png', dpi=500)