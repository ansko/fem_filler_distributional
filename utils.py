#!/usr/bin/env python3

''' Utilities for the calculations of the distributinal properties of the filler particles
'''


def extract_fillers_from_geo(fname):
    ''' From the .geo file, extract lines which define fillers, sort these lines, and return data
    '''
    fillers = dict()
    with open(fname) as f:
        lines = f.readlines()
        lines = [l.strip() for l in lines]
        lines = [l for l in lines if l]
        lines = [l for l in lines if l.startswith('solid filler_')]
        for l in lines:
            solid_filler, filler_line = l.split('=')
            filler_line = filler_line.strip()
            filler_name = solid_filler.split()[1]
            fillers[filler_name] = filler_line
    return fillers


def plane_line_to_geometry(plane_line):
    ''' Convert line (string) into data:
    plane(x, y, z; nx, ny, nz)
    '''
    data = plane_line[6:-1]
    xyz, nxnynz = data.split(';')
    x, y, z = xyz.split(',')
    x = float(x)
    y = float(y)
    z = float(z)
    nx, ny, nz = nxnynz.split(',')
    nx = float(nx)
    ny = float(ny)
    nz = float(nz)
    plane = {'a': nx, 'b': ny, 'c': nz, 'd': -nx*x - ny*y - nz*z, 'ptx': x, 'pty': y, 'ptz': z}
    return plane


def filler_to_planes(filler):
    ''' Parse filler into planes using following conventions.
        Filler is present in form of planes and orthobrick:
        -- planes[0] = top
        -- planes[1] = bottom
        -- planes[2:] = sides
        -- orthobrick = matrix
    '''
    planes = []
    # Filler is: plane and plane ... and plane and orthobrick <- which is matrix
    filler = filler.split('and orthobrick')[0].strip()
    if filler.startswith('(') and filler.endswith(')'):
        filler = filler[1:-1]
    else:
        raise ValueError("Something wrong with filler: " + str(filler))
    planes = filler.split('and')
    planes = [p.strip() for p in planes]
    planes = [plane_line_to_geometry(p) for p in planes]
    return planes


def three_planes_intersection_point(pl1, pl2, pl3):
    ''' Find and intersection point of three planes
    (which is used to find intersection of top/bottom, side_i and side_{i+1})
    '''
    # Kramer is for Ax=B, while here it is Ax + D = 0, so use minus sign
    a1 = pl1['a']; b1 = pl1['b']; c1 = pl1['c']; d1 = -pl1['d']
    a2 = pl2['a']; b2 = pl2['b']; c2 = pl2['c']; d2 = -pl2['d']
    a3 = pl3['a']; b3 = pl3['b']; c3 = pl3['c']; d3 = -pl3['d']
    det = a1*(b2*c3 - b3*c2) - b1*(a2*c3-a3*c2) + c1*(a2*b3-a3*b2)
    assert abs(det) > 1e-3
    det1 = d1*(b2*c3 - b3*c2) - b1*(d2*c3-d3*c2) + c1*(d2*b3-d3*b2)
    det2 = a1*(d2*c3 - d3*c2) - d1*(a2*c3-a3*c2) + c1*(a2*d3-a3*d2)
    det3 = a1*(b2*d3 - b3*d2) - b1*(a2*d3-a3*d2) + d1*(a2*b3-a3*b2)
    x = det1/det
    y = det2/det
    z = det3/det
    point = {'x': x, 'y': y, 'z': z}
    return point


def planes_to_points(planes):
    ''' Convert planes into theirs intersection points
    '''
    points = []
    top = planes[0]
    bottom = planes[1]
    # Iterate: side_i, side_{i+1}
    for idxa in range(2, len(planes)):
        if idxa < len(planes) - 1:
            idxb = idxa + 1
        else:
            idxb = 2
        points.append(three_planes_intersection_point(top, planes[idxa], planes[idxb]))
        points.append(three_planes_intersection_point(bottom, planes[idxa], planes[idxb]))
    return points


def points_center(points):
    ''' Calculate geometrical center of many points
    '''
    xc = yc = zc = 0
    for pt in points:
        xc += pt['x']
        yc += pt['y']
        zc += pt['z']
    xc /= len(points)
    yc /= len(points)
    zc /= len(points)
    return {'x': xc, 'y': yc, 'z': zc}


def extract_bbox_from_geo(fname):
    ''' Matrix is defined as orthobrick, which may be considered as bbox of the composite
    '''
    with open(fname) as f:
        lines = f.readlines()
        lines = [l.strip() for l in lines]
        lines = [l for l in lines if l]
        lines = [l for l in lines if l.startswith('solid matrix')]
        assert len(lines) == 1
        geometry_data = lines[0].split('=')[1].strip()
        geometry_data = geometry_data[11:]  # remove 'orthobrick('
        geometry_data = geometry_data.split(')')[0]
        bottom, top = geometry_data.split(';')
        xb, yb, zb = bottom.split(',')
        xb = float(xb)
        yb = float(yb)
        zb = float(zb)
        xt, yt, zt = top.split(',')
        xt = float(xt)
        yt = float(yt)
        zt = float(zt)
    bbox = {'xlo': xb, 'ylo': yb, 'zlo': zb, 'xhi': xt, 'yhi': yt, 'zhi': zt}
    return bbox


class CylinderRestored:
    ''' Containder for data
    '''
    def __init__(self, points):
        self.points = points
        self.center = points_center(points)
        self.top_points = points[0::2]
        self.top_center = points_center(self.top_points)
        self.bottom_points = points[1::2]
        self.bottom_center = points_center(self.bottom_points)


def distance(pt1, pt2, bbox):
    ''' Find the distance between two points considering preiodicity
    '''
    lx = bbox['xhi'] - bbox['xlo']
    ly = bbox['yhi'] - bbox['ylo']
    lz = bbox['zhi'] - bbox['zlo']
    dx = abs(pt1['x'] - pt2['x'])
    dy = abs(pt1['y'] - pt2['y'])
    dz = abs(pt1['z'] - pt2['z'])
    dx = min(dx, lx-dx)
    dy = min(dy, ly-dy)
    dz = min(dz, lz-dz)
    return (dx**2 + dy**2 + dz**2)**0.5


def diameter(points, bbox):
    ''' Calculate diameter of a group of points, which is maximal distance between points
    '''
    d = distance(points[0], points[1], bbox)
    for idx1, pt1 in enumerate(points):
        for idx2 in range(idx1+1, len(points)):
            d = max(d, distance(pt1, points[idx2], bbox))
    return d


def cos_angle_between_cylinders(cyl1, cyl2):
    ''' Find an angle between normals of two cylinders
    '''
    dx1 = cyl1.bottom_center['x'] - cyl1.top_center['x']
    dy1 = cyl1.bottom_center['y'] - cyl1.top_center['y']
    dz1 = cyl1.bottom_center['z'] - cyl1.top_center['z']
    dx2 = cyl2.bottom_center['x'] - cyl2.top_center['x']
    dy2 = cyl2.bottom_center['y'] - cyl2.top_center['y']
    dz2 = cyl2.bottom_center['z'] - cyl2.top_center['z']
    product = dx1*dx2 + dy1*dy2 + dz1*dz2
    l1 = (dx1**2 + dy1**2 + dz1**2)**0.5
    l2 = (dx2**2 + dy2**2 + dz2**2)**0.5
    cos_angle = product / l1 / l2
    return cos_angle


def msq(xs, ys):
    ''' Perform an approximation of data with linear function using mean-square minimization
    '''
    n = len(xs)
    sx = sum(xs)
    sy = sum(ys)
    sxy = sum([xs[i]*ys[i] for i in range(n)])
    sx2 = sum([x**2 for x in xs])
    a = (n*sxy - sx*sy)/(n*sx2 - sx**2)
    b = (sy - a*sx)/n
    return a, b


def distance_between_cylinders_centers(cyl1, cyl2, bbox):
    return distance(cyl1.center, cyl2.center, bbox)


def find_index_of_closest_centers_cylinder(idx, cylinders, bbox):
    ''' From the cylinders, find index of the cylinder closest (center) to that with index idx,
    which all are placed inside bounding box bbox
    '''
    # Find a distance to start with
    if idx < len(cylinders) - 1:
        d_min = distance(cylinders[idx].center, cylinders[idx + 1].center, bbox)
        idx_min = idx + 1
    else:
        d_min = distance(cylinders[idx].center, cylinders[idx - 1].center, bbox)
        idx_min = idx - 1

    for idx2, cyl2 in enumerate(cylinders):
        if idx2 == idx:
            continue
        d = distance(cylinders[idx].center, cyl2.center, bbox)
        if d < d_min:
            d_min = d
            idx_min = idx2
    return idx_min


if __name__ == '__main__':
    ...
